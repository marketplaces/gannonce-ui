FROM node:9.11.2 as build-front
WORKDIR /app
COPY . .
RUN echo 'module.exports = { "baseUrl": "" }' > src/lib/conf.js
RUN rm -rf /app/node_modules
RUN npm install -g yarn@1.22.18
RUN yarn
RUN ./node_modules/brunch/bin/brunch build --production

FROM nginx as run-front
RUN apt update
RUN apt install curl vim -y
COPY --from=build-front /app/www /var/www/html
COPY --from=build-front /app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
